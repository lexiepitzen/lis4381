# LIS4381

## Lexie Pitzen

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions

#### README.md file should include the following items:

* Screenshot of ampps installation running
* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App
* git commands w/ short descriptions
* bitbucket repo links

#### Git commands w/short descriptions:

1. git init - creates new repository
2. git status - status of directory including changes
3. git add - add change to staging area
4. git commit - saves changes and takes snapshot
5. git push - sends local repo data to remote repo
6. git pull - fetches data from remote repo
7. git config - configurates git settings

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](ampps_screenshot.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](android_screenshot.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](java_screenshot.png)


#### Tutorial Links:

*Bitbucket Station Locations:*
[Bitbucket Station Locations Link](https://bitbucket.org/lexiepitzen/lis4381/src/master/stationlocations)
