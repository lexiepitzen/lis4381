# LIS4381

## Lexie Pitzen

### Assignment #3 Requirements:

* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Links to A3.mwb and A3.sql files

#### File Links:
* [A3.mwb](A3.mwb "A3 ERD")
* [A3.sql](A3.sql "A3 SQL")

#### ERD Screenshot:

![A3 ERD Screenshot](A3ERD.png)

#### App Screenshots:

![Interface 1](Interface_1.png "Interface 1") ![Interface 2](Interface_2.png "Interface 2")

#### Skillset Screenshots:
![Skillset 4](skillset_4_screenshot.png "Skillset 4") ![Skillset 5](skillset_5_screenshot.png "Skillset 5") ![Skillset 6](skillset_6_screenshot.png "Skillset 6") 




