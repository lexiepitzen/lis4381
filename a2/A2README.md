# LIS4381

## Lexie Pitzen

### Assignment #2 Requirements:

* Recipe app screenshots
* Skillset screenshots

#### Recipe App Screenshots:

![First Interface](bruschetta_recipe.png "First Interface") ![Second Interface](recipe_text.png "Second Interface")

#### Skillset Screenshots

![Skillset 1](skillset_1_screenshot.png "Skillset 1") ![Skillset 2](skillset_2_screenshot.png "Skillset 2") ![Skillset 3](skillset_3_screenshot.png "Skillset 3") 