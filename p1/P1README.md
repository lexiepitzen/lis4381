# LIS4381

## Lexie Pitzen

### Project #1 Requirements:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Skillset screenshots


#### App Screenshots:

![Interface 1](P1_Interface_1.png "Interface 1") ![Interface 2](P1_Interface_2.png "Interface 2")

#### Skillset Screenshots:
![Skillset 7](skillset_7_screenshot.png "Skillset 7") ![Skillset 8](skillset_8_screenshot.png "Skillset 8") ![Skillset 9](skillset_9_screenshot.png "Skillset 9") 