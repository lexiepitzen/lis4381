# LIS4381

## Lexie Pitzen

### Project #2 Requirements:

* Review index.php code
* Add server-side validation and regular expressions
* Screenshots of failed validation, before and after successful edit, deleted record, RSS feed

#### Home Page Carousel:

![Home Page Carousel](homepagecarousel.png "Home Page Carousel")

#### index.php:

![index.php](indexphp.png "index.php")

#### edit_petstore.php:

![edit_petstore](editpetstorephp.png "edit_petstore")

#### Failed Validation:

![Failed Validation](failedvalidation.png "Failed Validation")

#### Passed Validation:

![Passed Validation](passedvalidation.png "Passed Validation")

#### Delete Record Prompt:

![Delete Record Prompt](deleterecordprompt.png "Delete Record Prompt")

#### Successfully Deleted Record:

![Successfully Deleted Record](successfullydeletedrecord.png "Successfully Deleted Record")

#### RSS Feed:

![RSS Feed](rssfeed.png "RSS Feed")






