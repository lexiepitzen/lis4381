# LIS4381

## Lexie Pitzen

### Assignment #5 Requirements:

* Review index.php code
* Add server-side validation and regular expressions
* Provide skillset screenshots

#### Link to local web app:
* [http://localhost/lis4381/index.php](http://localhost/lis4381/index.php "Web app")


#### Application Screenshots:
![Error Message](errormessage.png "Error Message")![Index.php file](petstoretable.png "Index.php file")


#### Skillset Screenshots:
![Skillset 13](skillset_13_screenshot.png "Skillset 13") ![Simple Calculator Index.php](simplecalculatorindexphp.png "Simple Calculator Index.php") ![Simple Calculator Process.php](simplecalculatorprocessphp.png "Simple Calculator Process.php") ![Read/Write Index.php](readwriteindexphp.png "Read/Write Index.php") ![Read/Write Process.php](readwriteprocessphp.png "Read/Write Process.php")





