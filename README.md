# LIS4381

## Lexie Pitzen

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/A1README.md "My A1 README.md file")
	* Install AMPPS
	* Install JDK
	* Install Android Studio and create My First App
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
	* Provide git command descriptions
2. [A2 README.md](a2/A2README.md "My A2 README.md file")
	* Create Healthy Recipes Android app
	* Provide screenshots of completed app
	* Provide screenshots of skillsets
3. [A3 README.md](a3/A3README.md "My A3 README.md file")
	* Create ERD based upon business rules
	* Provide screenshot of completed ERD
	* Provide screenshots of app interfaces
	* Provide links to files
4. [A4 README.md](a4/A4README.md "My A4 README.md file")
	* Clone assignment files
	* Review index.php code
	* Create favicon and place in each assignment's main directory
	* Provide screenshots of skillsets
5. [A5 README.md](a5/A5README.md "My A5 README.md file")
	* Review index.php code
	* Add server-side validation and regular expressions
	* Provide skillset screenshots
6. [P1 README.md](p1/P1README.md "My P1 README.md file")
	* Create resume app
	* Provide screenshots of app interfaces
	* Provide screenshots of skillsets
7. [P2 README.md](p2/P2README.md "My P2 README.md file")
	* Review index.php code
	* Add server-side validation and regular expressions
	* Provide screenshots
