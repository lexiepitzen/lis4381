# LIS4381

## Lexie Pitzen

### Assignment #4 Requirements:

* Clone assignment files
* Review index.php code
* Create favicon and place in each assignment's main directory
* Provide screenshots of running application
* Provide screenshots of skillsets

#### Link to local web app:
* [http://localhost/lis4381/index.php](http://localhost/lis4381/index.php "Web app")


#### Application Screenshots:
![Carousel](carousel1.png "Carousel")![Unsuccessful validation](unsuccessfulvalidation.png "Unsuccessful validation") ![Successful validation](successfulvalidation.png "Successful validation")


#### Skillset Screenshots:
![Skillset 10](skillset_10_screenshot.png "Skillset 10") ![Skillset 11](skillset_11_screenshot.png "Skillset 11") ![Skillset 12](skillset_12_screenshot.png "Skillset 12")




